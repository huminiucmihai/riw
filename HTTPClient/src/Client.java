import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URL; 

public class Client {
	
	public static ArrayDeque<CrawlerUrl> urlQueue = new ArrayDeque<CrawlerUrl>();
	
	public static String CreateHttpRequest(URL url, String ipAddress) {
		String hostName = url.getHost();
		String path = url.getPath();
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("GET " + (path.length() == 0 ? "/" : path) + " " + "HTTP/1.1" + "\r\n");
		sb.append("Host: " + ipAddress + "\r\n");
		sb.append("User-Agent: CLIENT_RIW\r\n");
		sb.append("Connection: close\r\n");
		sb.append("\r\n");
		
		return sb.toString();
	}
	
	public static void RequestRobots(URL url, CrawlerUrl crawlerUrl) throws IOException {
		Socket s = new Socket(crawlerUrl.hostName ,80);
		
		PrintWriter pw = new PrintWriter(s.getOutputStream());
		
		
		String request = CreateHttpRequest(url, crawlerUrl.hostName);
        pw.print(request);
        pw.flush();
        
        BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
        FileWriter writer;
        File f;
        Boolean returnedError = false;
        
        String responseLine = br.readLine();
        
    	if(!responseLine.split(" ")[1].equals("200") ) { //is error
    		returnedError = true;
    		f = new File("./" + crawlerUrl.hostName);
    		f.mkdirs(); //create dirs if they don't exist
    		//write request that generated error
    		writer = new FileWriter(f.getAbsolutePath() + "/error.txt");
    		writer.write(request);
    		writer.write("\r\n");
    		//write the first line of the response
    		writer.write(responseLine + "\n");
    	} else {
    		f = new File(crawlerUrl.hostName);
    		f.mkdirs(); //create dirs to the file if they don't exist
    		writer = new FileWriter(f.getAbsolutePath() + "/robots.txt");
    		while((responseLine = br.readLine()) != null) {
    			if(responseLine.length() == 0) {
    				break;
    			}
    		}
    	}
    	
    	if(returnedError) {
	        while((responseLine = br.readLine()) != null) {
	        	writer.write(responseLine);
	        }
    	} else {
    		 while((responseLine = br.readLine()) != null) {
    			 writer.write(responseLine + "\n");
 	        }
    	}
    	
        s.close();
        writer.close();
	}
	
	public static void HttpRequest(CrawlerUrl crawlerUrl) throws UnknownHostException, IOException {
		Socket s = new Socket(crawlerUrl.ipAddress ,80);
		URL url = crawlerUrl.PopUrl();
		
		PrintWriter pw = new PrintWriter(s.getOutputStream());
		
		String request = CreateHttpRequest(url, crawlerUrl.hostName);
        pw.print(request);
        pw.flush();
        
        BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
        FileWriter writer;
        File f;
        Boolean returnedError = false;
        StringBuilder sb = new StringBuilder();
        
        String responseLine = br.readLine();
        
    	if(!responseLine.split(" ")[1].equals("200") ) { //is error
    		returnedError = true;
    		f = new File(crawlerUrl.hostName + "/" + url.getPath());
    		f.mkdirs(); //create dirs if they don't exist
    		//write request that generated error
    		writer = new FileWriter(f.getAbsolutePath() + "/error.txt");
    		writer.write(request);
    		writer.write("\r\n");
    		//write the first line of the response
    		writer.write(responseLine);
    	} else {
        	f = new File(crawlerUrl.hostName + "/" + url.getPath()); 
    		f.mkdirs(); //create dirs to the file if they don't exist
    		writer = new FileWriter(f.getAbsolutePath() + "/index.html");
    		while((responseLine = br.readLine()) != null) {
    			if(responseLine.contains("<html")) {
    				sb.append(responseLine);
    				break;
    			}
    		}
    	}
    	
    	if(returnedError) {
	        while((responseLine = br.readLine()) != null) {
	        	writer.write(responseLine);
	        	sb.append(responseLine);
	        }
	        s.close();
	        writer.close();
	        return;
    	} else {
    		 while((responseLine = br.readLine()) != null) {
 	        	sb.append(responseLine);
 	        }
    	}
    	
    	String urlString = url.toString();
    	if(urlString.endsWith(".html")) {
    		urlString = urlString.replace(".html", "/");
    	}
    	Document doc = Jsoup.parse(sb.toString(), urlString);
    	Element metaRobots = doc.select("meta[name=robots]").first();
    	
    	List<String> robotAttributes;
    	
    	if(metaRobots == null) {
    		robotAttributes = new ArrayList<String>();
    		robotAttributes.add("all");
    	} else {
    		robotAttributes = Arrays.asList(metaRobots.attr("content").replace(",", "").split(" "));
    	}
    	
    	if(robotAttributes.contains("all") || robotAttributes.contains("index") || robotAttributes.size() == 0) {
    		writer.write(sb.toString());
    	}
    	
    	if(robotAttributes.contains("all") || robotAttributes.contains("follow") || robotAttributes.size() == 0) {
            ExtractLinks(doc);
    	}
        
        writer.close();
        pw.close();
        s.close();
	}

	public static void ExtractLinks(Document document) {
		Elements links = document.select("a");
		for(Element link : links) {
			URL url;
			try {
				String urlString = link.absUrl("href");
				if(urlString.isEmpty()) {
					continue;
				}
				url = new URL(urlString);
				File urlIndexPath = new File(url.getHost() + url.getPath() + "index.html");
				File urlErrorPath = new File(url.getHost() + url.getPath() + "error.txt");
				boolean visited = urlIndexPath.exists() || urlErrorPath.exists();
				if(!visited) {
					boolean added = false;
					for(CrawlerUrl i : urlQueue) {
						if(i.hostName.equals(url.getHost())) {
							i.AddUrlToQueue(url);
							added = true;
							break;
						}
					}
					if(!added) {
						CrawlerUrl crawlerUrl = new CrawlerUrl(url);
						try {
							RequestRobots(new URL(url.getProtocol() + "://" + crawlerUrl.hostName + "/robots.txt"), crawlerUrl);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						urlQueue.add(crawlerUrl);
					}
				} else {
					System.out.println(urlString + " already visited!");
				}
				
			} catch (MalformedURLException e) {
				System.out.println("Malformed URL! " + (link.attr("abs:href")));
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		URL startUrl = new URL("http://riweb.tibeica.com/crawl/");
		CrawlerUrl crawlerStart = new CrawlerUrl(startUrl);
		RequestRobots(new URL(startUrl.getProtocol() + "://" + crawlerStart.hostName + "/robots.txt"), crawlerStart);
		crawlerStart.CheckRobots();
		urlQueue.add(crawlerStart);
		
		while(urlQueue.size() > 0) {
			CrawlerUrl nextCrawlerUrl = urlQueue.peek();
			if(nextCrawlerUrl.CheckTime()) {
				if(!nextCrawlerUrl.hasRobots) {
					nextCrawlerUrl.CheckRobots();
				}
				boolean foundUrl = nextCrawlerUrl.PlaceToPop();
				if(foundUrl) {
					HttpRequest(nextCrawlerUrl);
					if(nextCrawlerUrl.GetQueueCount() > 0) {
						urlQueue.add(nextCrawlerUrl);
					}
				}
			} else {
				urlQueue.add(nextCrawlerUrl);
			}
			urlQueue.pop();
			
		}		
	}

}
