import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RobotChecker {
	
	public static List<String> GetAllowList(String documentPath) {
		List<String> allow = new ArrayList<String>();
		
		File doc = new File(documentPath);
		
		if(!doc.exists()) {
			return allow;
		}
		
		BufferedReader br;
		
		try {
			br = new BufferedReader(new FileReader(doc));
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			return null;
		}
		
		String line;
		boolean foundCrawler = false;
		boolean foundSpecific = false;
		
		try {
			while((line = br.readLine()) != null) {
				if(foundCrawler || foundSpecific) {
					if(line.contains("Allow:")) {
						if(line.replace("Allow:", "").trim().length() > 0)
						allow.add(line.replace("Allow:", "").trim());
					}
				}
				
				if(line.contains("User-agent: *")) {
					if(foundSpecific) {
						break;
					}
					foundCrawler = true;
				}
				
				if(line.contains("User-agent: RIWEB_CRAWLER")) {
					foundSpecific = true;
				}
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		return allow;
	}
	
	public static List<String> GetDisallowList(String documentPath) {
		List<String> disallow = new ArrayList<String>();
		
		File doc = new File(documentPath);
		
		if(!doc.exists()) {
			return disallow;
		}
		
		BufferedReader br;
		
		try {
			br = new BufferedReader(new FileReader(doc));
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			return null;
		}
		
		String line;
		boolean foundCrawler = false;
		boolean foundSpecific = false;
		
		try {
			while((line = br.readLine()) != null) {
				if(foundCrawler || foundSpecific) {
					if(line.contains("Disallow:")) {
						if(line.replace("Disallow:", "").trim().length() > 0)
						disallow.add(line.replace("Disallow:", "").trim());
					}
				}
				
				if(line.contains("User-agent: *")) {
					if(foundSpecific) {
						break;
					}
					foundCrawler = true;
				}
				
				if(line.contains("User-agent: RIWEB_CRAWLER")) {
					foundSpecific = true;
				}
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		return disallow;
	}

}
