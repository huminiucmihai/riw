import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.time.LocalTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class CrawlerUrl {
	
	public String hostName;
	public String ipAddress;
	public boolean hasRobots;
	LocalTime lastAccessed;
	LocalTime nextAccess;
	
	List<String> disallowed;
	List<String> allowed;
	
	Deque<URL> urlQueue = new ArrayDeque<URL>();
	
	public CrawlerUrl(URL url) {
		hostName = url.getHost();
		disallowed = new ArrayList<String>();
		allowed = new ArrayList<String>();
		urlQueue = new ArrayDeque<URL>();
		lastAccessed = LocalTime.now();
		urlQueue.add(url);
		hasRobots = false;
		try {
			ipAddress = InetAddress.getByName(hostName).getHostAddress();
		} 
		catch (UnknownHostException e) 
		{
			System.out.println(e);
		}
	}
	
	public int GetQueueCount() {
		return urlQueue.size();
	}
	
	public void AddUrlToQueue(URL url) {
		urlQueue.add(url);
	}
	
	public boolean PlaceToPop() {
		while (urlQueue.size() > 0)
		{
			if(CheckUrl()) {
				lastAccessed = LocalTime.now();
				return true;
			} else {
				urlQueue.pop();
			}
		}
		return false;
	}
	
	public URL PopUrl() {
		return urlQueue.pop();
	}
	
	public boolean CheckTime() {
		return lastAccessed.plusSeconds(1).compareTo(LocalTime.now()) < 0;
	}
	
	public void CheckRobots() {
		this.allowed = RobotChecker.GetAllowList(hostName +  "/robots.txt");
		this.disallowed = RobotChecker.GetDisallowList(hostName +  "/robots.txt");
	}
	
	private boolean CheckUrl() {
		URL url = urlQueue.peek();
		boolean accessible = true;
		
		String path = "/" + url.getPath();
		
		if(allowed.size() > 0) {
			for(String allow : allowed) {
				if(path.contains(allow)) {
					accessible = true;
					break;
				}
			}
		}
		
		if(disallowed.size() > 0) {
			for(String disallow : disallowed) {
				if(path.contains(disallow)) {
					accessible = false;
					break;
				}
			}
		}
		
		System.out.println(url.getHost() + url.getPath() + " is " + (accessible ? "allowed" : "not allowed"));
		
		return accessible;
	}
}
