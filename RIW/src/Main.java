import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class Main {

	
	public static void main(String[] args) throws IOException {				
		//Indexer.CreateDirectIndex();
		//Indexer.CreateIndirectIndexBatch();
		
		Scanner in = new Scanner(System.in);
		
		System.out.print("Insert query string - ");
		
		String query = in.nextLine();
		
		in.close();
		
		List<String> results = SearchModule.Search(query); //compet come
		
		if(results.size() == 0) {
			System.out.println("No results found!");
			return;
		}
		
		for(String result : results) {
			System.out.println(result);
		}
	}
}