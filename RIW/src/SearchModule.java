import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;

public class SearchModule {
	
	public static List<String> Search(String query) throws IOException {
		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		
		List<String> result = new ArrayList<String>();
		List<String> list = new ArrayList<String>();
		
		String[] queryWords = query.split(" ");
		
		File stopWordsFile = new File(Constants.stopWordsFilePath);
		File exceptionsFile = new File(Constants.exceptionsFilePath);
		Stemmer stem = new Stemmer();
		
		
		//prepare exception and stop lists
		String line;
		BufferedReader br;
		ArrayList<String> stopWords = new ArrayList<String>();
		ArrayList<String> exceptions = new ArrayList<String>();
		
		br = new BufferedReader(new FileReader(stopWordsFile));
		while((line = br.readLine())!= null) {
			stopWords.add(line);
		}
		br.close();
		
		br = new BufferedReader(new FileReader(exceptionsFile));
		while((line = br.readLine())!= null) {
			exceptions.add(line);
		}
		br.close();
		//preparation done
		
		int j=0;
		int opCode;
		
		boolean isListPopulated = false;
		while(j < queryWords.length && !isListPopulated) {	
			if(exceptions.contains(queryWords[j])) {
				isListPopulated = true;
			} else if(stopWords.contains(queryWords[j])) {
				j++;
				continue;
			} else {
				stem.add(queryWords[j].toCharArray(), queryWords[j].length());
				stem.stem();
				queryWords[j] = stem.toString();
				isListPopulated = true;
			}
			result = LoadDocsList(queryWords[j], mongoClient);
			j++;
		}
		for(int i = j; i < queryWords.length; i++) {
			if(queryWords[i].startsWith("!")) { // ! = not
				opCode = 3;
				queryWords[i] = queryWords[i].substring(1, queryWords[i].length());
			} else {
				if(queryWords[i].startsWith("?")) { // ? = or
					opCode = 2;
					queryWords[i] = queryWords[i].substring(1, queryWords[i].length());
				} else {
					opCode = 1; //nothing means and
				}
			}
			
			if(exceptions.contains(queryWords[i])) {
				list = LoadDocsList(queryWords[i], mongoClient);
				result = Operate(opCode, result, list);
			} else if(stopWords.contains(queryWords[i])) {
				continue;
			} else {
				stem.add(queryWords[i].toCharArray(), queryWords[i].length());
				stem.stem();
				queryWords[i] = stem.toString();
				list = LoadDocsList(queryWords[i], mongoClient);
				result = Operate(opCode, result, list);
			}
		}
		
		result = CosDistance(result, Arrays.asList(queryWords), mongoClient);
		
		mongoClient.close();
		Collections.reverse(result);
		return result;
	}
	
	private static List<String> Operate(int opCode, List<String> list1, List<String> list2){		
		switch(opCode) {
			case 1:
				return AndOp(list1, list2);
			case 2:
				return OrOp(list1, list2);
			case 3:
				return NotOp(list1, list2);
			default:
				return new ArrayList<String>();
		}
	}
	
	private static List<String> LoadDocsList(String word, MongoClient mongoClient){
		
		List<String> result = new ArrayList<String>();
		
		MongoDatabase db = mongoClient.getDatabase("ProiectRIW");
		MongoCollection<Document> indirectIndexCollection = db.getCollection("IndirectIndex");
		try {
			
		Document term = indirectIndexCollection.find(Filters.eq("term", word)).first();
		
		List<Document> docs = (List<Document>) term.get("docs");
		
		for (Document doc : docs){
			result.add(doc.getString("d"));
		}
		return result;
		} catch (Exception e) {
			return new ArrayList<String>();
		}
	}
	
	private static List<String> OrOp(List<String> list1, List<String> list2){
		List<String> answer = new ArrayList<String>();
		
		if(list1.size() > list2.size()) {
			answer.addAll(list1);
			
			for(String doc : list2) {
				if(!answer.contains(doc)) {
					answer.add(doc);
				}
			}
		} else {
			answer.addAll(list2);
			
			for(String doc : list1) {
				if(!answer.contains(doc)) {
					answer.add(doc);
				}
			}
		}
		return answer;
	}
	
	private static List<String> AndOp(List<String> list1, List<String> list2){
		List<String> answer = new ArrayList<String>();
		
		if(list1.size() > list2.size()) {
			for(String doc : list2) {
				if(list1.contains(doc)) {
					answer.add(doc);
				}
			}
		} else {
			for(String doc : list1) {
				if(list1.contains(doc)) {
					answer.add(doc);
				}
			}
		}	
		return answer;
	}
	
	private static List<String> NotOp(List<String> list1, List<String> list2){
		List<String> answer = new ArrayList<String>();
		
		for(String doc : list1) {
			if(!list2.contains(doc)) {
				answer.add(doc);
			}
		}	
		return answer;
	}

	private static List<String> CosDistance(List<String> list, List<String> queryWords, MongoClient mongoClient) {
		
		List<String> result = new ArrayList<String>();
		TreeMap<Double, String> tm= new TreeMap<Double, String>();
		
		Hashtable<String, Double> queryTFIDF = CalculateQueryTfIdf(queryWords, mongoClient);
		double queryNumitor = 0;
		
		for(String key : queryTFIDF.keySet()) {
			queryNumitor += Math.pow(queryTFIDF.get(key), 2);
		}
		queryNumitor = Math.sqrt(queryNumitor);
				
		MongoDatabase db = mongoClient.getDatabase("ProiectRIW");
		MongoCollection<Document> directIndexCollection = db.getCollection("DirectIndex");
		
		long totalDocuments = directIndexCollection.countDocuments();
		
		if(list.size() == 0) 
			return new ArrayList<String>();
		
		for(String doc : list) {
			
			double numarator = 0;
			double numitor = 0;
			
			for(String word : queryWords) {
				System.out.println(word);
				Document mongoDoc = directIndexCollection.find(Filters.eq("doc", doc))
						.projection(Projections.fields(Projections.include("terms.c", "terms.tf"), Projections.excludeId(), 
								Projections.elemMatch("terms", Filters.eq("t", word)))).first();
				
				double wordTF = ((List<Document>) mongoDoc.get("terms")).get(0).getDouble("tf");
				double wordIDF = Math.log((double)directIndexCollection.countDocuments() / (double)directIndexCollection.countDocuments(Filters.eq("terms.t", word)));
				double wordTFIDF = wordTF * wordIDF;
				//System.out.println("Word TF " + wordTF);
				//System.out.println("Word IDF " + wordIDF);
				//System.out.println("Word TF IDF " + wordTFIDF);
				numarator = numarator + queryTFIDF.get(word) * wordTFIDF;
				numitor = numitor + Math.pow(wordTFIDF, 2);
				
				//System.out.println("numarator " + numarator);
				//System.out.println("numitor " + numitor);
				
				//TO DO change so hat numitor is fucking ALL TFT-IDFS!!!!!!
			}
			
			numitor = Math.sqrt(numitor);
			
			//System.out.println("Sqrt(numitor) " + numitor);
			//System.out.println("something weird " + numitor * queryNumitor);
			
			double cosDistance = numarator/(numitor * queryNumitor);
			
			System.out.println(cosDistance + "  " + doc);
			
			while(tm.containsKey(cosDistance)) {
				cosDistance += 0.0000001;
			}
			tm.put(cosDistance, doc);
		}
		
		Set set = tm.entrySet();
		Iterator i = set.iterator();
		while(i.hasNext()) {
			Map.Entry me = (Map.Entry)i.next();
			result.add((String)me.getValue());
		}
		
		return result;
	}
	
	private static Hashtable<String, Double> CalculateQueryTfIdf(List<String> query, MongoClient mongoClient){
		
		MongoDatabase db = mongoClient.getDatabase("ProiectRIW");
		MongoCollection<Document> directIndexCollection = db.getCollection("DirectIndex");
		
		Hashtable<String, Double> queryTable = new Hashtable<String, Double>();
		
		for(String word : query) {
			if(queryTable.contains(word)) {
				double value = queryTable.get(word) + 1;
				queryTable.replace(word, value);
			} else {
				queryTable.put(word, 1.0);
			}
		}
		
		for(String key : queryTable.keySet()) {
			queryTable.replace(key, queryTable.get(key)/query.size());
		}
		
		for(String word : query) {
			double IDF = Math.log(1 + (double)directIndexCollection.countDocuments(Filters.eq("terms.t", word))/(double)directIndexCollection.countDocuments());
			
			queryTable.replace(word, queryTable.get(word) * IDF);
		}
		
		return queryTable;
	}


}
