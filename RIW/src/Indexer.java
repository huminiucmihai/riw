import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.bson.Document;

import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOneModel;

import static com.mongodb.client.model.Updates.push;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.BulkWriteException;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class Indexer {
		
	public static void CreateDirectIndex() throws IOException {
		
		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase db = mongoClient.getDatabase("ProiectRIW");
		MongoCollection<Document> collection = db.getCollection("DirectIndex");
		MongoCollection<Document> indirectIndexCollection = db.getCollection("ForIndirectIndex");
		List<Document> docTitles = new ArrayList<Document>();
		List<Document> terms;
		Document doc;
		
		int totalWords = 0;
		double tf = 0;

		File stopWordsFile = new File(Constants.stopWordsFilePath);
		File exceptionsFile = new File(Constants.exceptionsFilePath);
		File workDirectory = new File(Constants.originalFilesPath);
		//File directMapFile = new File(Constants.directMapFile);
		Stemmer stem = new Stemmer();
		
		String line;
		BufferedReader br;
		ArrayList<String> stopWords = new ArrayList<String>();
		ArrayList<String> exceptions = new ArrayList<String>();
		
		Hashtable<String, Integer> ht = new Hashtable<String, Integer>();
		
		br = new BufferedReader(new FileReader(stopWordsFile));
		while((line = br.readLine())!= null) {
			stopWords.add(line);
		}
		br.close();
		
		br = new BufferedReader(new FileReader(exceptionsFile));
		while((line = br.readLine())!= null) {
			exceptions.add(line);
		}
		br.close();
		
		Queue<File> directoryQueue = new LinkedList<File>();
		directoryQueue.add(workDirectory);
		
		while(!directoryQueue.isEmpty()) {
			File currentDir = directoryQueue.remove();
			File[] dirContent = currentDir.listFiles();
			for(File currentFile : dirContent) {
				if(currentFile.isDirectory()) {
					directoryQueue.add(currentFile);
				} 
				else 
				{
					br = new BufferedReader(new FileReader(currentFile));
					int c;
					StringBuilder sb = new StringBuilder("");
					while((c = br.read())!= -1) {
						char ch = Character.toLowerCase((char)c);
						if((ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9')) {
							sb.append(ch);
						} else {
							if(sb.length() > 1){
								String word = sb.toString();
								if(exceptions.contains(word)) {
									//just add because exception
									if(ht.containsKey(word)) {
										int value = ht.get(word);
										ht.remove(word);
										ht.put(word, value+1 );
									} else {
										ht.put(word, 1);
									}
									totalWords++;
								} else 	if(stopWords.contains(word)) {
									sb.setLength(0);
									continue;
								} else {
									totalWords++;
									stem.add(word.toCharArray(), word.length());
									stem.stem();
									word = stem.toString();
									
									if(ht.containsKey(word)) {
										int value = ht.get(word);
										ht.remove(word);
										ht.put(word, value+1 );
									} else {
										ht.put(word, 1);
									}
								}
							}
							sb.setLength(0);
						}
					}
					br.close();
					
					String filename = getFileNameWithoutExtension(currentFile);
					doc = new Document();
					terms = new ArrayList<Document>();
					
					doc.append("doc", filename);
					doc.append("path", currentFile.getAbsolutePath());
					docTitles.add(new Document("title", filename));
					
					
					for(String key: ht.keySet()){
						tf = (double)ht.get(key)/(double)totalWords;
						terms.add(new Document()
								.append("t", key)
								.append("c", ht.get(key))
								.append("tf", tf));
					}
					
					ht.clear();
					doc.append("terms", terms);
					collection.insertOne(doc);
				}
			}
		}
		indirectIndexCollection.insertMany(docTitles);
		mongoClient.close();
	}	
	
	public static void CreateIndirectIndexBatch() throws IOException {
	
		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase db = mongoClient.getDatabase("ProiectRIW");
		MongoCollection<Document> collection = db.getCollection("DirectIndex");
		MongoCollection<Document> forIndirectIndexCollection = db.getCollection("ForIndirectIndex");
		MongoCollection<Document> indirectIndexCollection = db.getCollection("IndirectIndex");
		
		BulkWriteOptions bulkWriteOptions = new BulkWriteOptions();
		bulkWriteOptions.ordered(false);
		bulkWriteOptions.bypassDocumentValidation(true);
		
		UpdateOptions updateOptions = new UpdateOptions();
	    updateOptions.upsert(true); //if true, will create a new doc in case of unmatched find
	    updateOptions.bypassDocumentValidation(true); //set true/false
		
		MongoCursor<Document> cursor = forIndirectIndexCollection.find().iterator();
		List<UpdateOneModel<Document>> updateDocuments = new ArrayList<UpdateOneModel<Document>>();
		
		System.out.println("Started indirect index creation at " + new Timestamp(new Date().getTime()));
		
		while(cursor.hasNext()) {
			String title = (String)cursor.next().get("title");
			Document docDI = collection.find(Filters.eq("doc", title)).first();			
			List<Document> docWords = (List<Document>)docDI.get("terms");
			
			
			for (Document word : docWords) {
				
				Document filterDocument = new Document();
			    filterDocument.append("term", (String)word.get("t"));
			    
			    Document updateDocument = new Document();
			    Document pushDocument = new Document("docs", new Document()
			    		.append("d", title)
						.append("c", (int)word.get("c")));
			    updateDocument.append("$push", pushDocument);

			    updateDocuments.add(
			            new UpdateOneModel<Document>(
			                    filterDocument,
			                    updateDocument,
			                    updateOptions));
			}
		}
		
		
		try {
		    //Perform bulk update
			indirectIndexCollection.bulkWrite(updateDocuments,
		            bulkWriteOptions);
		} catch (BulkWriteException e) {
		    //Handle bulkwrite exception
		    System.out.println(e.getMessage());
		}
		
		forIndirectIndexCollection.drop();
		mongoClient.close();
		
		System.out.println("Done with indirect index at " + new Timestamp(new Date().getTime()) );
		
	}
	
	private static String getFileNameWithoutExtension(File file) {
        String fileName = "";
        try {
            if (file != null && file.exists()) {
                String name = file.getName();
                fileName = name.replaceFirst("[.][^.]+$", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
            fileName = "";
        }
        return fileName;
    }
}
