import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;

public class DnsClient {
	
	static int dnsRequestBaseLength = 18;
	static int udpPort = 53;
	
	public static byte[] SetUpDnsRequest(URL url) {
		
		String host = url.getHost();
		System.out.println(host);
		int requestLength = dnsRequestBaseLength + host.length();
		byte[] request = new byte[requestLength];
		
		//id
		request[0] = 0x00;
		request[1] = 0x11;
		
		//flags
		request[2] = 0x00;
		
		//QD count
		request[5] = 0x01;
		
		//set up the question name
		int questionIndex = 12;
		String[] hostParts = host.split("\\.");
		for(String part : hostParts) {
			System.out.println(part.length());
			request[questionIndex] = (byte) part.length();
			questionIndex++;
			for(int i = 0; i<part.length(); i++) {
				request[questionIndex] = (byte)part.charAt(i);
				questionIndex++;
			}
		}
		request[questionIndex] = 0x00;
		
		//set up question type
		questionIndex+= 2;
		request[questionIndex] = 0x01;
		//set up question class
		questionIndex+= 2;
		request[questionIndex] = 0x01;
		
		return request;
	}
	
	
	public static void MakeDnsQuery(byte[] request, String host) throws IOException {
		DatagramSocket datagramSocket = new DatagramSocket();
		InetAddress address = InetAddress.getByName(host);
		DatagramPacket requestPacket = new DatagramPacket(request, request.length, address, udpPort);
		datagramSocket.send(requestPacket);
			
		byte[] response = new byte[512];
		
		DatagramPacket responsePacket = new DatagramPacket(response	, 512);
		datagramSocket.receive(responsePacket);
	}
	
	
	public static void main(String[] args) throws IOException {
		byte[] test = SetUpDnsRequest(new URL("http://www.google.com"));
		for(int i = 0; i < test.length; i++) {
			if(i%8 ==0) {
				System.out.println();
			}
			System.out.printf("0x%02X  ", test[i]);
		}
	}

}
